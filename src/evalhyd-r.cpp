/*! \file */ 
#include <vector>
#include <array>
#include <string>
#include <unordered_map>

#include <xtl/xoptional.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xview.hpp>
#include <xtensor-r/rarray.hpp>

#include <Rcpp.h>

#include "evalhyd/evald.hpp"
#include "evalhyd/evalp.hpp"

// [[Rcpp::plugins(cpp14)]]

// -----------------------------------------------------------------------------
// PREPROCESSING
// -----------------------------------------------------------------------------

// Function to manually convert bootstrapping parameter R List to C++ map
std::unordered_map<std::string, int> convert_bootstrap(Rcpp::Nullable<Rcpp::List> bootstrap)
{
    std::unordered_map<std::string, int> bootstrap_;
  
    if (bootstrap != R_NilValue)
    {
        Rcpp::List bts(bootstrap);
        if (bts.containsElementNamed("n_samples"))
        {
            bootstrap_["n_samples"] = bts["n_samples"];
        }
        if (bts.containsElementNamed("len_sample"))
        {
            bootstrap_["len_sample"] = bts["len_sample"];
        }
        if (bts.containsElementNamed("summary"))
        {
            bootstrap_["summary"] = bts["summary"];
        }
    }
    
    return bootstrap_;
}

// Function to manually convert conditional masking parameter from R matrix to xtensor
xt::xtensor<std::array<char, 32>, 2> convert_masking_conditions(Rcpp::Nullable<Rcpp::StringMatrix> m_cdt)
{
    if (m_cdt != R_NilValue)
    {
        auto cdt = Rcpp::as<Rcpp::StringMatrix>(m_cdt);
      
        xt::xtensor<std::array<char, 32>, 2> m_cdt_ (
            {static_cast<std::size_t>(cdt.nrow()), 
             static_cast<std::size_t>(cdt.ncol())}
        );
      
        for (int i = 0; i < cdt.nrow(); i++)
        {
            for (int j = 0; j < cdt.ncol(); j++)
            {
                std::string s = Rcpp::as<std::string>(cdt(i, j));
                
                std::array<char, 32> a ({});
                std::copy(s.begin(), s.end(), a.data());

                xt::view(m_cdt_, i, j) = a;
            }
        }
        
        return m_cdt_;
    }
    else
    {
        return xt::xtensor<std::array<char, 32>, 2> ({});
    }
}

// -----------------------------------------------------------------------------


/// Function to evaluate deterministic streamflow predictions.
// [[Rcpp::export(".evald_cpp")]]
std::vector<xt::rarray<double>> evald_cpp(
    SEXP q_obs, 
    SEXP q_prd, 
    Rcpp::StringVector metrics,
    SEXP q_thr = R_NilValue,
    Rcpp::Nullable<Rcpp::String> events = R_NilValue,
    Rcpp::Nullable<Rcpp::String> transform = R_NilValue, 
    Rcpp::Nullable<double> exponent = R_NilValue, 
    Rcpp::Nullable<double> epsilon = R_NilValue, 
    Rcpp::Nullable<xt::rarray<double>> t_msk = R_NilValue,
    Rcpp::Nullable<Rcpp::StringMatrix> m_cdt = R_NilValue,
    Rcpp::Nullable<Rcpp::List> bootstrap = R_NilValue,
    Rcpp::Nullable<Rcpp::StringVector> dts = R_NilValue,
    Rcpp::Nullable<int> seed = R_NilValue,
    Rcpp::Nullable<Rcpp::StringVector> diagnostics = R_NilValue
) 
{
    // manually convert bootstrapping parameter R List to C++ map
    std::unordered_map<std::string, int> bootstrap_ = convert_bootstrap(bootstrap);
  
    // manually convert conditional masking parameter from R matrix to xtensor
    xt::xtensor<std::array<char, 32>, 2> m_cdt_ = convert_masking_conditions(m_cdt);
    
    // check type of data structures
    if (Rf_isMatrix(q_obs) and Rf_isMatrix(q_prd))
    {
        // nothing to do, only here to catch matrices 
        // early, since a matrix is also a vector
    }
    else if (Rf_isVector(q_obs) and Rf_isVector(q_prd))
    {
        // reshape vectors to turn them 2-dimensional
        // (note: the reshaping impacts both obs/prd,
        //  and q_obs/q_prd)
        auto obs = Rcpp::as<Rcpp::NumericVector>(q_obs);
        auto prd = Rcpp::as<Rcpp::NumericVector>(q_prd);
        
        obs.attr("dim") = Rcpp::Dimension(1, obs.length());
        prd.attr("dim") = Rcpp::Dimension(1, prd.length());
    }
    
    if (q_thr != R_NilValue)
    {
        if (Rf_isMatrix(q_thr))
        {
            // nothing to do, only here to catch matrices 
            // early, since a matrix is also a vector
        }
        else
        {
            // reshape vector to turn it 2-dimensional
            auto thr = Rcpp::as<Rcpp::NumericVector>(q_thr);
            thr.attr("dim") = Rcpp::Dimension(1, thr.length());
        }
    }
  
    // compute metrics
    try
    {
        std::vector<xt::xarray<double>> res = evalhyd::evald(
            xt::rarray<double> (q_obs),
            xt::rarray<double> (q_prd),
            Rcpp::as<std::vector<std::string>>(metrics),
            (
                (q_thr == R_NilValue) ? 
                xt::rarray<double> (xt::zeros<double>({0,0})) : 
                xt::rarray<double> (q_thr)
            ),
            (
                (events == R_NilValue) ? 
                xtl::missing<Rcpp::String>() : 
                Rcpp::as<Rcpp::String>(events)
            ),
            (
                (transform == R_NilValue) ? 
                xtl::missing<Rcpp::String>() : 
                Rcpp::as<Rcpp::String>(transform)
            ),
            (
                (exponent == R_NilValue) ? 
                xtl::missing<double>() : 
                Rcpp::as<double>(exponent)
            ),
            (
                (epsilon == R_NilValue) ? 
                xtl::missing<double>() : 
                Rcpp::as<double>(epsilon)
            ),
            (
                t_msk.isNull() ?
                xt::rarray<double> (xt::zeros<double>({0,0,0})) :
                xt::rarray<double> (t_msk)
            ),
            (
                (m_cdt == R_NilValue) ?
                xt::xtensor<std::array<char, 32>, 2> ({}) :
                m_cdt_
            ),
            (
                (bootstrap == R_NilValue) ? 
                xtl::missing<std::unordered_map<std::string, int>>() : 
                bootstrap_
            ),
            (
                (dts == R_NilValue) ?
                Rcpp::as<std::vector<std::string>>(Rcpp::StringVector::create()) :
                Rcpp::as<std::vector<std::string>>(dts)
            ),
            (
                (seed == R_NilValue) ? 
                xtl::missing<int>() : 
                Rcpp::as<int>(seed)
            ),
            (
                (diagnostics == R_NilValue) ?
                xtl::missing<std::vector<std::string>>() :
                Rcpp::as<std::vector<std::string>>(diagnostics)
            )
        );
      
        std::size_t ret_size = metrics.size();
        if (diagnostics != R_NilValue)
        {
          ret_size += Rcpp::as<std::vector<std::string>>(diagnostics).size();
        }
      
        std::vector<xt::rarray<double>> ret;
        for (int i = 0; i < ret_size; i++)
        {
            ret.push_back(xt::rarray<double>(res[i]));
        }
    
        return ret;
    }
    catch (const Rcpp::eval_error& ex) 
    {	
        std::string ex_str = ex.what();
        Rcpp::stop(ex_str);
    }
}

/// Function to evaluate probabilistic streamflow predictions.
// [[Rcpp::export(".evalp_cpp")]]
std::vector<xt::rarray<double>> evalp_cpp(
    xt::rarray<double> q_obs,
    xt::rarray<double> q_prd,
    Rcpp::StringVector metrics,
    Rcpp::Nullable<xt::rarray<double>> q_thr = R_NilValue,
    Rcpp::Nullable<Rcpp::String> events = R_NilValue,
    Rcpp::Nullable<Rcpp::NumericVector> c_lvl = R_NilValue,
    Rcpp::Nullable<xt::rarray<double>> t_msk = R_NilValue,
    Rcpp::Nullable<Rcpp::StringMatrix> m_cdt = R_NilValue,
    Rcpp::Nullable<Rcpp::List> bootstrap = R_NilValue,
    Rcpp::Nullable<Rcpp::StringVector> dts = R_NilValue,
    Rcpp::Nullable<int> seed = R_NilValue,
    Rcpp::Nullable<Rcpp::StringVector> diagnostics = R_NilValue
)
{
    // manually convert bootstrapping parameter R List to C++ map
    std::unordered_map<std::string, int> bootstrap_ = convert_bootstrap(bootstrap);
  
    // manually convert conditional masking parameter from R matrix to xtensor
    xt::xtensor<std::array<char, 32>, 2> m_cdt_ = convert_masking_conditions(m_cdt);
  
    // compute metrics
    try 
    {
        std::vector<xt::xarray<double>> res = evalhyd::evalp(
            q_obs,
            q_prd,
            Rcpp::as<std::vector<std::string>>(metrics),
            (
                q_thr.isNull() ? 
                xt::rarray<double> (xt::zeros<double>({0, 0})) : 
                xt::rarray<double> (q_thr)
            ),
            (
                (events == R_NilValue) ? 
                xtl::missing<Rcpp::String>() : 
                Rcpp::as<Rcpp::String>(events)
            ),
            (
                (c_lvl == R_NilValue) ? 
                Rcpp::as<std::vector<double>>(Rcpp::NumericVector::create()) : 
                Rcpp::as<std::vector<double>>(c_lvl)
            ),
            (
                t_msk.isNull() ? 
                xt::rarray<double> (xt::zeros<double>({0,0,0,0})) : 
                xt::rarray<double> (t_msk)
            ),
            (
                (m_cdt == R_NilValue) ?
                xt::xtensor<std::array<char, 32>, 2> ({}) :
                m_cdt_
            ),
            (
                (bootstrap == R_NilValue) ? 
                xtl::missing<std::unordered_map<std::string, int>>() : 
                bootstrap_
            ),
            (
                (dts == R_NilValue) ? 
                Rcpp::as<std::vector<std::string>>(Rcpp::StringVector::create()) : 
                Rcpp::as<std::vector<std::string>>(dts)
            ),
            (
                (seed == R_NilValue) ? 
                xtl::missing<int>() : 
                Rcpp::as<int>(seed)
            ),
            (
                (diagnostics == R_NilValue) ?
                xtl::missing<std::vector<std::string>>() :
                Rcpp::as<std::vector<std::string>>(diagnostics)
            )
        );
      
        std::size_t ret_size = metrics.size();
        if (diagnostics != R_NilValue)
        {
            ret_size += Rcpp::as<std::vector<std::string>>(diagnostics).size();
        }

        std::vector<xt::rarray<double>> ret;
        for (int i = 0; i < ret_size; i++)
        {
            ret.push_back(xt::rarray<double>(res[i]));
        }
      
        return ret;
    } 
    catch (const Rcpp::eval_error& ex) 
    {	
          std::string ex_str = ex.what();
          Rcpp::stop(ex_str);
    }
}
