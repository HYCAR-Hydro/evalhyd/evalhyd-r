.. default-role:: obj

..
   latest
   ------

   Yet to be versioned and released. Only available from *dev* branch until then.

v0.1.2.0
--------

Released on 2024-01-22.

.. rubric:: Dependency changes

* move to `evalhyd-cpp==0.1.2`
  (`see changelog <https://hydrogr.github.io/evalhyd/cpp/changelog.html#v0-1-2>`_)

v0.1.1.0
--------

Released on 2023-06-16.

.. rubric:: Dependency changes

* move to `evalhyd-cpp==0.1.1`
  (`see changelog <https://hydrogr.github.io/evalhyd/cpp/changelog.html#v0-1-1>`_)

.. rubric:: Bug fixes

* fix memory leak for metrics relying on sorting function (i.e. ``QS``, 
  ``CRPS_FROM_QS``, ``CRPS_FROM_ECDF``, ``CRPS_FROM_BS``, ``AW``, ``AWN``, 
  ``WS``, ``RANK_HIST``, ``DS``, ``AS``)
  (`R#4 <https://gitlab.irstea.fr/HYCAR-Hydro/evalhyd/evalhyd-r/-/issues/4>`_)
* fix crashing conditional masking functionality on Windows for R versions 
  later than 4.1.3
  (`R#6 <https://gitlab.irstea.fr/HYCAR-Hydro/evalhyd/evalhyd-r/-/issues/6>`_)

.. rubric:: Repository changes

* refactor repository for submission to CRAN, in particular by changing 
  the way dependencies are vendored
  (`R#5 <https://gitlab.irstea.fr/HYCAR-Hydro/evalhyd/evalhyd-r/-/issues/5>`_)

v0.1.0.0
--------

Released on 2023-05-03.

* first release
